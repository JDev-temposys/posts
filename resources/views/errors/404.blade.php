@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="main-heading">404 Page not found!</h1>
    <img src="/img/404.jpg" style="width: 100%" alt="">
</div>
@endsection