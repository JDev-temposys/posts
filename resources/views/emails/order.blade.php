@component('mail::message')
# Thank you For your order!

Thank you for choosing us, we are the best!
<br>

Your payment was confirmed, here are the books, you've ordered.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
