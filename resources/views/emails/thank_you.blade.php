@component('mail::message')
# Thank you For your order!

Thank you for choosing us, we are the best!
<br>
Your order contains:

@component('mail::table')
    | Book title       | Author(s)    | Quantity | Price  |
    | ---------------- |:---------:| :-------:|:------ |
    @foreach($books as $book)
    | {{$book->title}} | @foreach($book->authors as $author) {{$author->name}}    @endforeach | {{$book->quantity}} | {{$book->price}} $ |
    @endforeach
    | | | Total |{{$total}} $ |
@endcomponent

When payment will be confirmed, you will automatically receive ordered books on this email address.


Thanks,<br>
{{ config('app.name') }}
@endcomponent
