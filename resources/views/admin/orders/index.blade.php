@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Orders
@endsection

@section('contentheader_title')
    Orders
@endsection

@section('menu_links')
    <li class="active"><i class="fa fa-money" aria-hidden="true"></i> Orders</li>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">
                <div class="box box-primary">
                    @if(\Session::has('message'))
                        <div class="alert alert-success" role="alert">
                            {!! Session::get('message') !!}
                        </div>
                    @endif
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-users table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>User Name</th>
                                    <th>Books</th>
                                    <th>Total books quantity</th>
                                    <th>Summ</th>
                                    <th>Payed</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if( isset($charges))
                                    @foreach($charges as $charge)
                                        <?php $total = 0?>
                                        <tr>
                                            <td>{!! $charge->id !!}</td>
                                            <td>{!! $charge->user->name !!}</td>
                                            <td>
                                                @foreach($charge->orders as $order)
                                                    <a href="{{route('admin.books.show', $order->book->id)}}">{{$order->book->title}}</a> ,
                                                    <?php $total += $order->quantity?>
                                                @endforeach
                                            </td>
                                            <td>{!! $total !!}</td>
                                            <td>{!! $charge->total !!}</td>
                                            <td>{!! $charge->payed ? 'Yes' : 'No' !!}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

