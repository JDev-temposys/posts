@extends('layouts.app')

@section('content')
<div class="container">
    <h2 class="header">Thank You!</h2>

    <p>Your order has been saved and waiting for payment confirmation!
        <br>
        When payment will be confirmed, you will automatically receive books on your email!
        <br>
        Copy of your order was sent to your email address.
        <br>Thanks for choosing Us!
    </p>

    <a href="{{route('home')}}" class="buy button-big">Home</a>

</div>
@endsection

