let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/css');
// mix.less('resources/assets/less/adminlte-app.less','public/css/adminlte-app.css');
// mix.less('node_modules/toastr/toastr.less','public/css/toastr.css');
   // .combine([
   //     'public/css/app.css',
   //     'node_modules/admin-lte/dist/css/skins/_all-skins.css',
   //     'public/css/adminlte-app.css',
   //     'node_modules/icheck/skins/square/blue.css',
   //     'public/css/toastr.css'
   // ], 'public/css/all.css')
mix.combine([
       'public/css/bootstrap.css',
       'resources/assets/css/main.css'
   ], 'public/css/all-landing.css');
   //APP RESOURCES
mix.copy('resources/assets/img/*.*','public/img');
mix.copy('resources/assets/js/*','public/js');
   //VENDOR RESOURCES
mix.copy('node_modules/font-awesome/fonts/*.*','public/fonts/');
mix.copy('node_modules/ionicons/dist/fonts/*.*','public/fonts/');
mix.copy('node_modules/admin-lte/bootstrap/fonts/*.*','public/fonts/bootstrap');
mix.copy('node_modules/admin-lte/dist/css/skins/*.*','public/css/skins');
mix.copy('node_modules/admin-lte/dist/img','public/img');
mix.copy('node_modules/admin-lte/plugins','public/plugins');
mix.copy('node_modules/icheck/skins/square/blue.png','public/css');
mix.copy('node_modules/icheck/skins/square/blue@2x.png','public/css');
