<?php

namespace App\Http\Controllers;

use App\Mail\OrderPayed;
use App\models\Charge;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class WebhookController extends Controller
{
    public function handle(Request $request)
    {
        $response = $request->all();

        \Log::info($request->all());

        if (strpos($response['type'], 'charge') === false) {               //if not a charge - just return 'ok' and do nothing
            return response('Not a charge',200);
        }

        $charge = Charge::where('stripe_charge_id', $response['data']['object']['id'])->first();
        if (!$charge) {
            return response('no charge with this id',200);
        }
        $paid = $response['data']['object']['paid'];
        $books = new Collection();
        if ($paid) {
            $charge->payed = $paid;
            $charge->save();
            foreach ($charge->orders as $order) {
                $book = $order->book;
                $book->quantity = $order->quantity;
                $books->push($book);
            }
        }

        $user = $charge->user;

        //sending order to user
        \Mail::to($user->email)
            ->later(Carbon::now()->addMinute(), new OrderPayed($books));
        return response('Ok',200);
    }
}