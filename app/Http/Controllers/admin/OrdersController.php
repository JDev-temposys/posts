<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\models\Charge;
use App\User;

class OrdersController extends Controller
{
    public function index()
    {
        $charges = Charge::with(['orders.book', 'orders', 'user'])->get();
        return view('admin.orders.index', ['user' => \Auth::user(), 'charges' => $charges]);
    }

}
