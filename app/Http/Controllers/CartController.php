<?php

namespace App\Http\Controllers;

use App\Mail\ThankYou;
use App\models\Charge;
use App\models\Order;
use App\models\StripeDetails;
use App\Repositories\BookRepository;
use Illuminate\Http\Request;

class CartController extends Controller
{
    protected $bookRepository;


    public function __construct(BookRepository $bookRepository)
    {
        $this->middleware('auth');
        $this->bookRepository = $bookRepository;
    }

    public function show()
    {
        $books = [];
        foreach (\Cart::getContent() as $item) {
            $book = $this->bookRepository->find($item->id);
            $book->quantity = $item->quantity;
            $books[] = $book;
        }
        return view('cart',['books' => $books]);
    }

    public function updateStripeDetails(Request $request)
    {
        $user = \Auth::user();
        if ($request->has('stripeToken')) {
            try {
                $cu = \Stripe::customers()->create([
                    'email' => $user->email,
                    'source' => $request->get('stripeToken')
                ]);
                $details = $user->stripeDetails;
                if (!$details) {
                    $details = StripeDetails::create([
                        'user_id' => $user->id
                    ]);
                    $details->save();
//                    dd($details);
                }

                $details->stripe_id = $cu['id'];
                $card = $cu['sources']['data'][0];
                $details->card_expired_at = $card['exp_month'] . '/' . $card['exp_year'];
                $details->card_brand = $card['brand'];
                $details->card_last_four = $card['last4'];
                $details->save();

                $success = "Your card details have been updated!";
            } catch (\Cartalyst\Stripe\Exception\StripeException $e) {
                $error = $e->getMessage();
                return redirect()->back()->withErrors(['error' => $error]);
            }
        }
        return redirect()->back();
    }

    public function buy()
    {
        $user = \Auth::user();

        $stripeCharge = \Stripe::charges()->create([
            'customer' => $user->stripeDetails->stripe_id,
            'currency' => 'USD',
            'amount'   => \Cart::getTotal()
        ]);

        $charge = new \App\models\Charge([
            'user_id' => $user->id,
            'stripe_charge_id' => $stripeCharge['id'],
            'total' => \Cart::getTotal()
        ]);
        $charge->save();

        $books = [];

        foreach (\Cart::getContent() as $item) {
            $order = new Order([
               'user_id' => $user->id,
               'book_id' => $item->id,
               'quantity' => $item->quantity,
               'charge_id' => $charge->id
            ]);
            $order->save();
            $book = $this->bookRepository->with('authors')->find($item->id);
            $book->quantity = $item->quantity;
            $books[] = $book;
        }
        \Cart::clear();

        \Mail::to($user->email)->send(new ThankYou($books ,\Cart::getTotal()));

        return view('thanks');

    }

    public function addToCart(Request $request)
    {
        $id = $request->get('productId');
        $book = $this->bookRepository->find($id);

        \Cart::add(array(
            'id' => $id,
            'name' => $book->title,
            'price' => $book->price,
            'quantity' => 1,
        ));

        return \Cart::getTotalQuantity();
    }

    public function addItem(Request $request)
    {
        $id = $request->get('productId');
        \Cart::update($id, [
            'quantity' => 1,
        ]);

        $data = [
            'total' => \Cart::getTotal(),
            'quantity' => \Cart::get($id)->quantity,
            'id' => $id,
            'cart' => \Cart::getTotalQuantity(),
            'summ' =>\Cart::get($id)->getPriceSum()
        ];

        return $data;
    }

    public function deductItem(Request $request)
    {
        $id = $request->get('productId');
        \Cart::update($id, [
            'quantity' => -1,
        ]);

        $data = [
            'total' => \Cart::getTotal(),
            'quantity' => \Cart::get($id)->quantity,
            'id' => $id,
            'cart' => \Cart::getTotalQuantity(),
            'summ' =>\Cart::get($id)->getPriceSum()
        ];

        return $data;
    }

    public function deleteItem(Request $request)
    {
        $id = $request->get('productId');
        \Cart::remove($id);

        $data = [
            'cart' => \Cart::getTotalQuantity(),
            'total' => \Cart::getTotal(),
            'id' => $id,
        ];

        return $data;
    }

    public function clearCart()
    {
        \Cart::clear();
        return redirect()->back();
    }

    public function getTotalQuantity()
    {
        return \Cart::getTotalQuantity();
    }
}
