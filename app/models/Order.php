<?php

namespace App\models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Order extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'user_id',
        'book_id',
        'charge_id',
        'quantity'
    ];

    public function book()
    {
        return $this->belongsTo(Book::class, 'book_id');
    }

    public function charge()
    {
        return $this->belongsTo(Charge::class, 'charge_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
