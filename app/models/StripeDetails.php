<?php

namespace App\models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class StripeDetails extends Model
{
    protected $fillable = [
        'user_id',
        'stripe_id',
        'card_expired_at',
        'card_brand',
        'card_last_four'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
