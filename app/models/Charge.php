<?php

namespace App\models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    protected $fillable = [
        'user_id',
        'stripe_charge_id',
        'payed',
        'total'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
