<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ThankYou extends Mailable
{
    use Queueable, SerializesModels;

    protected $books;
    protected $total;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($books, $total)
    {
        $this->books = $books;
        $this->total = $total;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.thank_you', ['books' => $this->books, 'total' => $this->total]);
    }
}
