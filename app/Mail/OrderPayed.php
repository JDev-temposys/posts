<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderPayed extends Mailable
{
    use Queueable, SerializesModels;

    protected $books;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($books)
    {
        $this->books = $books;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->view('emails.order')->subject('Your order');
        foreach ($this->books as $book) {
            $email->attach(\Storage::disk('books')->path($book->file));
        }
        return $email;
    }
}
